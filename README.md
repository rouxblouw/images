# images

A repo for public build images that I use.

## Images
  * `Simple` -> registry.gitlab.com/rouxblouw/images/simple
    * A play image that has a bit of everything. Not ideal for real apps.
  * `Infra` -> registry.gitlab.com/rouxblouw/images/infra
    * An image that contains the tools commonly used for infrastructure creation.
  * `SPA` -> registry.gitlab.com/rouxblouw/images/spa
    * A Nginx web server meant to be used to host SPA websites
  * `Containers` -> registry.gitlab.com/rouxblouw/images/containers
    * A Docker in Docker image to do docker builds and docker image scans using `scan ${IMAGE}`
    * The image scanning uses [Trivy](https://github.com/aquasecurity/trivy).
