#!/bin/bash
# 
# Scans a docker image using Trivy, and fails on critical detections
# Params:
#   $1 -> image name
set -e

trivy image --exit-code 0 --severity MEDIUM,HIGH "$1"
trivy image --exit-code 1 --severity CRITICAL "$1"